'use strict'

/*
 * DOM elements required
 */
var taskList = document.querySelector('.Task')
var textField = document.querySelector('input[type="text"]')
var button = document.querySelector('input[type="submit"]')

/*
 * Magic happens!
 */
button.addEventListener('click', addElement)

/*
 * Helpers
 */
function addElement (e) {
	e.preventDefault()

	if (textField.value == ' ' || textField.value == '') {
		return false;
	}

	var text = textField.value
	var textNode = document.createTextNode(text)

	textField.value = ''

	var element = document.createElement('li')

	element.appendChild(textNode)
	element.className = 'Task-element'

	taskList.appendChild(element)
}